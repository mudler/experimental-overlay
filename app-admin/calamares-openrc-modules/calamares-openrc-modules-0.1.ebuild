# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Calamares OpenRC modules"
HOMEPAGE="http://calamares.io"
SLOT="0"

MY_AUTHOR="mudler"
if [[ ${KDE_BUILD_TYPE} == live ]] ; then
	EGIT_REPO_URI="git://github.com/${MY_AUTHOR}/${PN}"
	KEYWORDS=""
else
	SRC_URI="https://github.com/${MY_AUTHOR}/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
	RESTRICT="mirror"
fi

LICENSE="GPL-3"
IUSE=""

RDEPEND="app-admin/calamares"

src_install() {
	insinto "/usr/lib/calamares/modules/"
	doins -r "${S}/"*
}
